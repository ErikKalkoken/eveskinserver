# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - yyyy-mm-dd

## [1.2.0] - 2023-09-02

### Change

- Migrate build process to PEP 621
- Upgrade dependencies

## [1.1.0] - 2020-12-26

### Change

- Will now return HTTP 400 (Bad Request) for invalid sizes, just like CCP's image server

## [1.0.0] - 2020-12-26

### Added

- Initial release
